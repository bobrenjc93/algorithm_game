$(document).ready(function() {

    (function (){

        var algorithms = {
            problems : [
                "Given variables A, B and C - Set A equal to the sum of B and C",
                "Given variables A, B - Set A equal to whether or not B is a palindrome",
                "Given variable A that contains a sentence, store the backwards sentence in B. ie. 'Walk the Dog' become 'Dog the Walk'"
            ],
            inits: [
                {A : [1,2,3,4,5], B : [5,4,3,2,1], C : [1,1,1,1,1]},
                {A : [0,0,0,0,0], B : ["bob","dad","boo","yoyoyoyoyo","naw"], C : [0,0,0,0,0]},
                {A : ["blah blah","walk the dog","dog the walk","hello",""], B : [0,0,0,0,0], C : [0,0,0,0,0]}
            ],
            tests : [
                {submission : ["A","A","A","A","A"], solution : [6,5,4,3,2]},
                {submission : ["A","A","A","A","A"], solution : [true,true,false,false,false]},
                {submission : ["B","B","B","B","B"], solution : ["blah blah","dog the walk","walk the dog","hello",""]}
            ],
            index : 0
        };

        window.Test = Backbone.Model.extend({

            defaults: function() {
                return {
                    index: Tests.nextIndex(),
                    result: 'Untested'
                };
            },

            update: function(result) {
                this.set({"result" : result});
                this.trigger("updated");
            }

        });

        window.TestResults = Backbone.Collection.extend({

            model: Test,

            runTests: function() {
                var code = $("#code")[0].value;
                var index = algorithms.index;
                var init = algorithms.inits[index];
                var passedAll = true;

                _(this.models).each(function(test){
                    var i = test.get('index');
                    var A = (init.A)[i], B = (init.B)[i], C = (init.C)[i];
                    var save = i; //save loop variable incase eval alters it... should fine better way to do this...
                    eval(code);
                    i = save;
                    var testData = algorithms.tests[index];
                    var result = eval('(' + (testData.submission)[i] + '=="' + (testData.solution)[i] + '")');
                    if (result) {
                        test.update("Passed");
                        window.AppView.updateScore(50);
                    }
                    else {
                        test.update("Failed");
                        passedAll = false;
                        window.AppView.updateScore(-50);
                    }
                }, this);

                if (passedAll) {
                    alert("Passed all tests! Onto the next algorithm!");
                    window.AppView.updateScore(1000);
                    window.AppView.resetEditor();
                    algorithms.index++;
                    if (algorithms.index === algorithms.problems.length) {
                        algorithms.index = 0;
                        alert("Finished all problems! Your final score is : " + window.AppView.score);
                    }
                    window.AppView.updateAlgorithm(algorithms.problems[algorithms.index]);
                    window.AppView.updateCompleted("Completed : " + algorithms.index);

                    this.resetAll();
                }
            },

            nextIndex: function() {
                if (!this.length) return 1;
                return this.last().get('index') + 1;
            },

            comparator: function(test) {
                return test.get('index');
            },

            resetAll: function() {
                _(this.models).each(function(test){
                    test.update("Untested");
                }, this);
            }

        });

        window.Tests = new TestResults();

        window.TestView = Backbone.View.extend({

            tagName: "li",
            className: "test",

            events: {
                //none atm
            },

            initialize: function() {
                _.bindAll(this, 'render','remove');

                this.model.bind('updated', this.render);
                this.model.bind('destroy', this.remove);
            },

            render: function() {
                $(this.el).html("Case " + (this.model.get('index') + 1) + " : " + this.model.get('result'));
                return this;
            },

            remove: function() {
                this.model.destroy();
            }

        });

        var timer = {
            start : new Date().getTime(),
            getTime : function () {
                current = new Date().getTime();
                return current - this.start;
            }
        };

        window.AppView = Backbone.View.extend({

            el: $("body"),

            events: {
                "click #submit"  : "runTests",
                "keydown #code"  : "runTestsOnEnter"
            },

            initialize: function() {
                _.bindAll(this, 'render', 'appendTest', 'runTestsOnEnter', 'runTests', 'updateAttempts',
                    'updateScore', 'resetEditor', 'updateAlgorithm', 'updateCompleted');

                $("#algorithm").text(algorithms.problems[algorithms.index]);

                var self = this;
                setInterval(function () {
                    var current = Math.round(timer.getTime()/1000);
                    $("#timer").text("Time : " + current + " s");
                    self.updateScore(-1);
                }, 1000);

                this.collection = new TestResults();

                for (var i = 0; i < 5; i++)
                    this.collection.add(new Test({index: i, result: "Untested"}));

                this.attempts = 0;
                this.score = 0;

                this.render();
            },

            render: function(){
                var self = this;
                $(this.el).append("<ul></ul>");
                _(this.collection.models).each(function(test){ // in case collection is not empty
                    self.appendTest(test);
                }, this);
            },

            appendTest: function(test){
                var TestView = new window.TestView({
                    model: test
                });
                $('#sidebar', this.el).append(TestView.render().el);
            },

            runTestsOnEnter: function(e) {
                var keyCode = (e.which ? e.which : e.keyCode);
                if (keyCode === 10 || keyCode === 13 && (e.ctrlKey || e.metaKey)) {
                    this.runTests();
                }
            },

            runTests: function() {
                this.collection.runTests();
                this.updateAttempts();
                $("#code").focus();
            },

            updateAttempts: function() {
                console.log("updating attempts");
                this.attempts++;
                $("#attempts").text("Attempts : " + this.attempts);
            },

            updateScore: function(amount) {
                this.score += amount;
                $("#score").text("Score : " + this.score);
            },

            resetEditor: function() {
                $("#code")[0].value = "";
            },

            updateAlgorithm: function(text) {
                $("#algorithm").text(text);
            },

            updateCompleted: function(text) {
                $("#completed").text(text);
            }

        });

        window.AppView = new AppView();

    }());

});